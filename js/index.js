$(function(){
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({
      interval: 4000
    })
  });

$('#modalContacto').on('show.bs.modal', function (e) {
    console.log('El modal contacto se esta mostrando');
    $('#contactoDPMF, #contactoDPE, #contactoDGIP, #contactoDEE, #contactoDI').removeClass('btn-outline-primary');
    $('#contactoDPMF, #contactoDPE, #contactoDGIP, #contactoDEE, #contactoDI').addClass('btn-secondary');
    $('#contactoDPMF, #contactoDPE, #contactoDGIP, #contactoDEE, #contactoDI').prop('disabled', true);
});

$('#modalContacto').on('shown.bs.modal', function (e) {
    console.log('El modal contacto se mostró');
});

$('#modalContacto').on('hide.bs.modal', function (e) {
    console.log('El modal contacto se oculta');
});

$('#modalContacto').on('hidden.bs.modal', function (e) {
    console.log('El modal contacto se ocultó');
    $('#contactoDPMF, #contactoDPE, #contactoDGIP, #contactoDEE, #contactoDI').prop('disabled', false);
    $('#contactoDPMF, #contactoDPE, #contactoDGIP, #contactoDEE, #contactoDI').removeClass('btn-secondary');
    $('#contactoDPMF, #contactoDPE, #contactoDGIP, #contactoDEE, #contactoDI').addClass('btn-outline-primary');
});